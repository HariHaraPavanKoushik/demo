package com.hcl.student.service;

import java.util.List;

import com.hcl.student.dto.StudentRequestDto;
import com.hcl.student.entity.RegisterCourse;
import com.hcl.student.entity.Student;
import com.hcl.student.exception.CustomException;

public interface StudentService {

	public String updateStudentDetails(StudentRequestDto studentRequestDto) throws CustomException;

	public String deleteStudentDetails(long studentId) throws CustomException;

	public String register(String studentName, String courseName) throws InstantiationException, IllegalAccessException;

	List<RegisterCourse> getCourseBySId(long id);

	public Student registerStudent(Student student);

	public List<Student> getAllStudentDetails();

	public Student getStudentByStudentId(long studentId);

	public List<RegisterCourse> getRegisteredStudentsByCourseName(String courseName);

}
