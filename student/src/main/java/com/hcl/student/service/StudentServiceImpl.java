package com.hcl.student.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.student.dto.StudentRequestDto;
import com.hcl.student.entity.RegisterCourse;
import com.hcl.student.entity.Student;
import com.hcl.student.exception.CustomException;
import com.hcl.student.repository.RegisterCourseRepository;
import com.hcl.student.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	RegisterCourseRepository registerCourseRepository;

	@Override
	public String updateStudentDetails(StudentRequestDto studentRequestDto) throws CustomException {
		Optional<Student> optionalStudent = studentRepository.findById(studentRequestDto.getStudentId());
		if (!optionalStudent.isPresent()) {
			throw new CustomException("Student does not exists");
		}
		Student studentDetails = optionalStudent.get();
		if (studentRequestDto.getStream() != null && !studentRequestDto.getStream().isEmpty())
			studentDetails.setStream(studentRequestDto.getStream());
		if (studentRequestDto.getStudentEmail() != null && !studentRequestDto.getStudentEmail().isEmpty())
			studentDetails.setStudentEmail(studentRequestDto.getStudentEmail());
		if (studentRequestDto.getStudentName() != null && !studentRequestDto.getStudentName().isEmpty())
			studentDetails.setStudentName((studentRequestDto.getStudentName()));
		if (studentRequestDto.getStudentPhoneNumber() != null && !studentRequestDto.getStudentPhoneNumber().isEmpty())
			studentDetails.setStudentPhoneNumber((studentRequestDto.getStudentPhoneNumber()));
		if (studentRepository.save(studentDetails) != null)
			return "Student details updated successfully!!!!";
		else
			return "Could not update Successfully!!!";
	}

	@Override
	public String deleteStudentDetails(long studentId) throws CustomException {
		Optional<Student> optionalStudent = studentRepository.findById(studentId);
		if (!optionalStudent.isPresent()) {
			throw new CustomException("Student does not exists");
		}
		studentRepository.deleteById(studentId);
		return "Student detail deleted successfully!!!";
	}

	@Override
	public String register(String studentName, String courseName)
			throws InstantiationException, IllegalAccessException {

		Student studentDetils = studentRepository.findByStudentName(studentName);
		System.out.println("studentDetils=====" + studentDetils);
		long studentId = studentDetils.getStudentId();
		System.out.println("studentId===== " + studentId);
		RegisterCourse registerCourse = new RegisterCourse();

		 String url = "http://localhost:8888/course/all";
		//String url = "http://localhost:8484/activecourses";
		Object[] objects = restTemplate.getForObject(url, Object[].class);

		System.out.println("object value ===== " + objects);

		List<Object> listOfCourses = Arrays.asList(objects);
		System.out.println("listofcourse===== " + listOfCourses);

		for (Object obj : listOfCourses) {
			Map<String, Object> myObjectAsDict = new HashMap<>();
			System.out.println("obj" + obj);
			Class<?> class1 = obj.getClass();

			ObjectMapper mapObject = new ObjectMapper();
			Map<String, Object> mapObj = mapObject.convertValue(obj, Map.class);

			Iterator iter = mapObj.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				System.out.println("[Key] : " + entry.getKey() + " [Value] : " + entry.getValue());

				System.out.println("entry.getValue().equals(courseName)====== " + entry.getValue().equals(courseName));
				if (entry.getValue().equals(courseName)) {
					registerCourse.setCourseName(courseName);
					registerCourse.setRegisteredDate(LocalDate.now());
					registerCourse.setStudentId(studentId);

					registerCourseRepository.save(registerCourse);

				}

			}

		}

		return "registered";
	}

	@Override
	public List<RegisterCourse> getCourseBySId(long id) {
		List<RegisterCourse> findAll = registerCourseRepository.findByStudentId(id);
		return findAll;

	}

	@Override
	public Student registerStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public List<Student> getAllStudentDetails() {
		return studentRepository.findAll();
	}

	@Override
	public Student getStudentByStudentId(long studentId) {
		return studentRepository.findByStudentId(studentId);
	}

	@Override
	public List<RegisterCourse> getRegisteredStudentsByCourseName(String courseName) {
		return registerCourseRepository.findByCourseName(courseName);
	}

}
