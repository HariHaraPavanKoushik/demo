package com.hcl.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.student.dto.StudentRequestDto;
import com.hcl.student.entity.RegisterCourse;
import com.hcl.student.entity.Student;
import com.hcl.student.exception.CustomException;
import com.hcl.student.service.StudentService;

@RequestMapping("/student")
@RestController
public class StudentController {

	@Autowired
	StudentService studentService;

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<String> updateStudentDetails(StudentRequestDto studentRequestDto) throws CustomException {
		return new ResponseEntity<String>(studentService.updateStudentDetails(studentRequestDto), HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{studentId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteStudentDetails(@PathVariable long studentId) throws CustomException {
		return new ResponseEntity<>(studentService.deleteStudentDetails(studentId), HttpStatus.OK);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<String> register(String studentName, String courseName)
			throws InstantiationException, IllegalAccessException {
		return new ResponseEntity<>(studentService.register(studentName, courseName), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/courseBySId/{id}")
	public ResponseEntity<List<RegisterCourse>> getCourseBySId(@PathVariable(name = "id") long id) {

		List<RegisterCourse> course = studentService.getCourseBySId(id);

		if (course != null) {
			return new ResponseEntity<List<RegisterCourse>>(course, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<RegisterCourse>>(course, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/savestudent")
	public ResponseEntity<Student> registerStudent(@RequestBody Student student) {
		return new ResponseEntity<Student>(studentService.registerStudent(student), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getAllStudentDetails")
	public ResponseEntity<List<Student>> getAllStudentDetails() {
		return new ResponseEntity<List<Student>>(studentService.getAllStudentDetails(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getStudentByStudentId/{studentId}")
	public ResponseEntity<Student> getStudentByStudentId(@PathVariable long studentId) {
		return new ResponseEntity<Student>(studentService.getStudentByStudentId(studentId), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getRegisteredStudentsByCourseName/{courseName}")
	public ResponseEntity<List<RegisterCourse>> getRegisteredStudentsByCourseName(@PathVariable String courseName) {
		return new ResponseEntity<List<RegisterCourse>>(studentService.getRegisteredStudentsByCourseName(courseName),
				HttpStatus.OK);
	}
}
