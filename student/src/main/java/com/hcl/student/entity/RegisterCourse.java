package com.hcl.student.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class RegisterCourse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long registerId;
	private long studentId;
	private String courseName;
	private LocalDate registeredDate;

}
