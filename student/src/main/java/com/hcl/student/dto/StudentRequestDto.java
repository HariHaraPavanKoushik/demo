package com.hcl.student.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentRequestDto {
	private long studentId;
	private String studentName;
	private String studentPhoneNumber;
	private String studentEmail;
	private String stream;
}
