package com.hcl.student.utility;

public class UserUtility {

	@SuppressWarnings("unused")
	private static final String SUCCESS = "Successful";
	
	@SuppressWarnings("unused")
	private static final String FAIL = "Failure";
	
	@SuppressWarnings("unused")
	private static final String AGAIN_SUCCESS = "Successful";

	@SuppressWarnings("unused")
	private static final String AGAIN_FAIL = "Failure";
	
	@SuppressWarnings("unused")
	private static final String TEST_AGAIN_FAIL = "Failure";
	
	@SuppressWarnings("unused")
	private static final String HARAN = "Failure";

}
