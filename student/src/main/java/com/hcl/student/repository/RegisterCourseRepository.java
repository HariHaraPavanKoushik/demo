package com.hcl.student.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.student.entity.RegisterCourse;

@Repository
public interface RegisterCourseRepository extends JpaRepository<RegisterCourse, Long> {
	List<RegisterCourse> findByStudentId(long id);

	public List<RegisterCourse> findByCourseName(String courseName);
}
