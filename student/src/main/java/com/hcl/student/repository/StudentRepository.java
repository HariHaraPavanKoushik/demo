package com.hcl.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.student.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	public Student findByStudentName(String studentName);

	public Student findByStudentId(long studentId);
}
